/*
* Helper utility to create react bootstrap basic inputs
* */

import React from 'react';
import {FormGroup, FormControl, ControlLabel, HelpBlock} from 'react-bootstrap';
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import momentPropTypes from 'react-moment-proptypes';
import './_DateRangePicker.scss';
//import PropTypes from 'prop-types';

export function FieldGroup({id, label, help, validationState, ...props}) {
  return (
    <FormGroup controlId={id} validationState={validationState}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      <FormControl.Feedback />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export function SelectGroup({ id, label, help, placeholder, options, onChange }) {
  function renderOptions(options) {
    //add an initial "Select" option with no value that will be disabled
    //TODO - how to disable an item
    const withEmptySelect = [{label:'Select',value:'',disabled:true}, ...options];
    return withEmptySelect.map((option, idx) => {return option.disabled ? <option key={idx} value={option.label} disabled>{option.label}</option> :
      <option key={idx} value={option.value}>{option.label}</option>});
  }
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl componentClass="select" placeholder={placeholder} onChange={onChange}>
        {/*TODO - find appropriate polyfill for isArray, if needed, instead of hasOwnProperty(whatever)*/}
        {/*cannot figure out how to import single lodash methods - not a declared dependency of the project, and not a modularized library, I don't think*/}
        {
          options && options.__proto__.hasOwnProperty('slice') && options[0].value && options[0].label
          && renderOptions(options)
        }
      </FormControl>
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export function DateRangePickerGroup ({id, label, help, validationState, ...props}) {
  return (
    <FormGroup controlId={id} validationState={validationState}>
      <ControlLabel>{label}</ControlLabel>
      <DateRangePicker {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

DateRangePickerGroup.propTypes = {
  startDate: momentPropTypes.momentObj,
  endDate: momentPropTypes.momentObj
};


