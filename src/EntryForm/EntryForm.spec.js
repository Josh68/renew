import React from 'react';
import ReactDOM from 'react-dom';
import EntryForm from './EntryForm';
import { shallow, mount, render } from 'enzyme';
import consts from '../App.constants';
import moment from 'moment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EntryForm />, div);
});

describe('Date range input behavior', () => {
  it('Renders initially with a date range starting 7 days ago and ending today', () => {
    const entryForm = render(<EntryForm/>);
    const startDateInput = entryForm.find('#' + consts.START_DATE);
    const endDateInput = entryForm.find('#' + consts.END_DATE);
    expect(startDateInput.val()).toEqual(moment().subtract(7,'days').format('YYYY-MM-DD'));
    expect(endDateInput.val()).toEqual(moment().format('YYYY-MM-DD'));
  });
});
