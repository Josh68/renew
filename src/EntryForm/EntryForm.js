import React, {Component} from 'react';
import {Row, Col, Button, Modal, ModalHeader, ModalTitle, ModalBody} from 'react-bootstrap';
import {FieldGroup, SelectGroup, DateRangePickerGroup} from '../FormComponents/FieldGroups';
import consts from '../App.constants';
import moment from 'moment';
import {isInclusivelyAfterDay} from 'react-dates';
import { Redirect, Link } from 'react-router';

const statesJson = require('../states_titlecase.json');

class EntryForm extends Component {
  constructor(props) {
    super(props);
    let focusedInput = null;
    if (props.autoFocus) {
      focusedInput = consts.START_DATE;
    } else if (props.autoFocusEndDate) {
      focusedInput = consts.END_DATE;
    }
    
    this.state = { //using local state at the form level instead of global state (no Redux, etc) in this sample project
      //TODO - wire in some kind of client persistence (sessionStorage) to handle page refreshes, wrapping setState
      //moment objects for the date range picker - are not used in form submission (see componentDidMount and onDatesChange)
      maxEndDate:moment().add(7,'days'),
      focusedInput,
      formControls:{
        startDate:moment().subtract(7,'days'), //minus one week is default start date
        endDate:moment(), //today is default end date
        //to hold all form control state
      }
    };
    
    //dynamically set initial address fields in state.formControls
    //supress direct state setting warning in this case
    Object.keys(consts.ADDRESS_FIELDS).forEach(prop => {
      /* eslint-disable react/no-direct-mutation-state */
      this.state.formControls[consts.ADDRESS_FIELDS[prop]] = '';
      /* eslint-enable react/no-direct-mutation-state */
    });
    
    //hold inputRefs for text fields, only, maybe as a way of doing validation
    this.formControls = {};
    
    this.onDatesFocusChange = this.onDatesFocusChange.bind(this);
    this.onDatesChange = this.onDatesChange.bind(this);
    this.dateRangeLimiter = this.dateRangeLimiter.bind(this);
    this.onAddrChange = this.onAddrChange.bind(this);
    this.stringFormatDate = this.stringFormatDate.bind(this);
  }
  
  //just a helper to create formatted string for form post
  stringFormatDate(moment) {
    return moment && moment.format && moment.format('YYYY-MM-DD');
  }
  
  //use to set initial state and do initial validations
  componentDidMount() {
    //initial validation
    //stub useless mock event to be checked by the container component's callback
    //distinguishes this callback from ones referencing the container element (App)
    this.props.checkFormIsValid && this.props.checkFormIsValid({nativeEvent:'componentDidMount'}, this.state.formControls);
  }
  
  //set state on changes to address fields
  onAddrChange(e) {
    console.log(e.target);
    const formControls = {...this.state.formControls};
    formControls[e.target.id] = e.target.value;
    this.setState({
      ...this.state,
      formControls
    });
  }
  
  //set state on changes to dates
  onDatesChange({ startDate, endDate }) {
    //isInclusivelyAfterDay includes the current day, so use 15 rather than 14 days (checked within dateRangeLimiter) - or not?
    const newEndDate = endDate && !this.dateRangeLimiter(endDate, startDate) ? endDate : moment(startDate).add(14,'days');
    const formControls = {...this.state.formControls};
    formControls[consts.START_DATE]=startDate;
    formControls[consts.END_DATE]=newEndDate;
    this.setState({
      ...this.state,
      formControls
    });
    console.log('onDatesChange', this);
    //change the input focus at this point - from start to end or vice-versa - to trigger correct setting of enforced date range
    this.onDatesFocusChange(this.state.focusedInput === consts.START_DATE ? consts.END_DATE : consts.START_DATE);
  }
  
  //mandatory(?) handler for changing from start to end date in date range picker
  onDatesFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }
  
  //callback to enforce date range limitations on picker component
  dateRangeLimiter(endDate, startDate) {
    const _startDate = startDate || this.state.formControls.startDate;
    //isInclusivelyAfterDay includes the current day, so use 15 rather than 14 days
    return isInclusivelyAfterDay(endDate, moment(_startDate).add(15,'days'));
  }
  
  // way to integrate optional use of client geolocation
  // useGeoLocation(continueFn) {
  //   if (!this.props.locationCapable) {
  //     return false;
  //   }
  //   continueFn(window.confirm('Allow detection of your current location?'));
  // }
  // decideFn(allow) {
  //   console.log('decision',allow);
  //   if (allow) {
  //
  //   } else {
  //
  //   }
  // }
  
  //render the form
  render() {
    //this.useGeoLocation(this.decideFn);
    return (
      // this.props.useGeolocation && this.props.hasChosenGeoOrForm && localStorage.getItem(consts.USE_GEOLOCATION) ?
      // <div>You chose to use geolocation!</div> :
      <div>
        <form noValidate onChange={(event) => this.props.checkFormIsValid(event.nativeEvent, this.state.formControls)} onSubmit={(event) => this.props.onSubmit(event, this)}>
          <Modal show={this.props.dataErrors && this.props.showErrorNotification} onHide={this.props.hideErrorNotification}>
            <Modal.Header closeButton>
              <Modal.Title>Houston, we have a problem</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>Data on your form may be incomplete or one of the online services needed to fetch your data may not be working. Please try again.</p>
            </Modal.Body>
          </Modal>
          <Row>
            <Col lg={7} md={7} sm={7} xs={12}>
              <FieldGroup
                id={consts.ADDRESS_FIELDS.STREET}
                name={consts.ADDRESS_FIELDS.STREET}
                type="text"
                label="Street address"
                value={this.state.formControls[consts.ADDRESS_FIELDS.STREET]}
                onChange={this.onAddrChange}
                inputRef={input => this.formControls[consts.ADDRESS_FIELDS.STREET] = input}
                help="Details that don't help pinpoint your location (e.g., apartment numbers) are unnecessary"
                required
                validationState={null}
              />
            </Col>
          </Row>
          <Row>
            <Col lg={3} md={3} sm={3} xs={12}>
              <FieldGroup
                id={consts.ADDRESS_FIELDS.CITY}
                name={consts.ADDRESS_FIELDS.CITY}
                type="text"
                label="City"
                value={this.state.formControls[consts.ADDRESS_FIELDS.CITY]}
                onChange={this.onAddrChange}
                inputRef={input => this.formControls[consts.ADDRESS_FIELDS.CITY] = input}
                required
                validationState={null}
              />
            </Col>
            <Col lg={2} md={2} sm={2} xs={6}>
              <SelectGroup
                id={consts.ADDRESS_FIELDS.STATE}
                name={consts.ADDRESS_FIELDS.STATE}
                label="State"
                value={this.state.formControls[consts.ADDRESS_FIELDS.STATE]}
                placeholder="Select"
                onChange={this.onAddrChange}
                required
                options={statesJson}
              />
            </Col>
            <Col lg={2} md={2} sm={2} xs={6}>
              <FieldGroup
                id={consts.ADDRESS_FIELDS.ZIP}
                name={consts.ADDRESS_FIELDS.ZIP}
                type="text"
                value={this.state.formControls[consts.ADDRESS_FIELDS.ZIP]}
                onChange={this.onAddrChange}
                label="ZIP Code"
                inputRef={input => this.formControls[consts.ADDRESS_FIELDS.ZIP] = input}
                required
                validationState={null}
              />
            </Col>
          </Row>
          <Row>
            <Col lg={7} md={7} sm={7} xs={7}>
              <DateRangePickerGroup
                id="DateFrom"
                label="Date From"
                startDate={this.state.formControls.startDate}
                endDate={this.state.formControls.endDate}
                onDatesChange={this.onDatesChange}
                focusedInput={this.state.focusedInput}
                onFocusChange={this.onDatesFocusChange}
                required
                isOutsideRange={this.dateRangeLimiter}
              />
            </Col>
          </Row>
          <Row>
            <Col lg={12} md={12} sm={12} xs={12}>
              {!this.props.formValidates ?
                <Button type="submit" disabled>Submit</Button> :
                (this.props.requestsInProgress ?
                  <Button disabled>Please wait...</Button> :
                  <Button type="submit">Submit</Button>
                )
              }
            </Col>
          </Row>
        </form>
        {
          this.props.navigateFromSubmit && this.props.history.push('/results')
        }
      </div>
    );
  }
}

export default EntryForm;