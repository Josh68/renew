import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import logo from './logo.svg';
import {Jumbotron, Grid, Navbar} from 'react-bootstrap';
import EntryForm from './EntryForm/EntryForm';
import SolarTimeTable from './SolarTimeTable/SolarTimeTable';
import consts from './App.constants';
import { hasLocalStorage, hasSessionStorage } from './services/storageTester';
import axios from 'axios';

import {getGeocoding, getTimezone, getSunriseSunset} from './services/apiService';

//moment and moment-range
import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideContent: true, //TODO - this is for development only
      dataErrors: false,
      showErrorNotification: false,
      requestsInProgress: false,
      navigateFromSubmit: false,
      addrFormIsValid: false,
      useGeolocation: false,
      geoLocation: {
        lat:'',
        lng:''
      },
      dayData: hasSessionStorage && sessionStorage.getItem(consts.TABLE_DATA) ? JSON.parse(sessionStorage.getItem(consts.TABLE_DATA)) : []
    };
    this.locationCapable = window.navigator && window.navigator.geolocation;
    this.onSubmit = this.onSubmit.bind(this);
    this.checkFormIsValid = this.checkFormIsValid.bind(this);
    this.hideErrorNotification = this.hideErrorNotification.bind(this);
    this.showContent = this.showContent.bind(this);
    this.enableReturnToForm = this.enableReturnToForm.bind(this);
  }
  
  componentDidMount() {
    //TODO - this is for development only
    setTimeout(this.showContent,100);
  }
  
  //TODO - this is for development only
  showContent() {
    this.setState({
      ...this.state,
      hideContent:false
    });
  }
  
  get hasChosenGeoOrForm() {
    return hasLocalStorage ? localStorage.getItem(consts.USE_GEOLOCATION) : false;
  }
  
  set useGeolocation(bool) {
    this.setState(this.state.useGeolocation, bool);
  }
  
  get useGeolocation() {
    return this.state.useGeolocation;
  }
  
  setDataErrors() {
    this.setState({
      ...this.state,
      dataErrors:true //TODO - flesh this out for real error handling
    });
  }
  
  showErrorNotification(e) {
    this.setState({
      ...this.state,
      dataErrors:true,
      showErrorNotification:true
    });
  }
  
  hideErrorNotification() {
    this.setState({
      ...this.state,
      showErrorNotification:false
    })
  }
  
  //this needs heavy refactoring to break it up
  onSubmit(e, formData) {
    e && e.preventDefault();
    console.log('submission', formData);
    console.log('this:', this);
    const data = formData && formData.state && formData.state.formControls ? formData.state.formControls : null;
    if (!data || !data[consts.ADDRESS_FIELDS.STREET] || data[consts.ADDRESS_FIELDS.STREET].trim().length === 0) { //dummy check for existence of a value for street
      this.showErrorNotification();
      return false;
    }
    //we're clear to start querying apis
    //set the form (button) to waiting state
    this.setState({
      ...this.state,
      requestsInProgress:true
    });
    //call Geocoding API
    getGeocoding(data)
      .then((response) => {
        const geoLocation = response.data;
        console.log('geoLocation', geoLocation);
        this.setState({
          ...this.state,
          geoLocation
        });
        return geoLocation;
      })
      .then((geoLocation) => {
        const dayRange = moment.range([data[consts.START_DATE], data[consts.END_DATE]]);
        const daysArray = Array.from(dayRange.by('day'));
        const dayData = daysArray.map((moment) => ({dayAsMoment: moment}));
        this.setState({
          ...this.state,
          dayData
        })
        return axios.all(daysArray.map((day) => {
          return getTimezone(geoLocation,day);
        }));
        // below, trying to pass back location and the array doesn't work, so far
        // return {
        //   days: axios.all(daysArray.map((day) => {
        //     return getTimezone(geoLocation,day);
        //   })),
        //   geoLocation
        // };
      })
      .then((timezoneResponse) => {
        //we have the timezone data (each with location points) by day
        //get just the data, nested in data
        const dayData = timezoneResponse.map((dayTzData, idx) => {
          return {...this.state.dayData[idx], ...dayTzData.data}
        });
        //set initial state of data by day
        this.setState({
          ...this.state,
          dayData
        });
        //since this is jsonp, need to set up a promise to chain and a counter on callbacks
        const sunriseSunsetArray = [];
        const sunriseSunsetPromise = new Promise((resolve, reject) => {
          let counter = 0;
          const sunriseSunsetCallback = (error, response) => {
            sunriseSunsetArray.push(response);
            if (counter < dayData.length - 1) {
              counter++;
            } else {
              //resolve the promise when all days' data have been fetched
              resolve(sunriseSunsetArray);
            }
          };
          //call the api for each day
          dayData.map((day) => {
            const date = day.dayAsMoment.format('YYYY-MM-DD');
            getSunriseSunset(day.location, date, sunriseSunsetCallback);
          });
        });
        //return the promise
        return sunriseSunsetPromise;
      })
      .then((sunriseSunsetResponse) => {
        //we have the sunrise-sunset data, by day
        //get just the data, nested in results
        const sunriseSunsetData = sunriseSunsetResponse.map((daySunriseSunset) => daySunriseSunset.results);
        const dayData =  sunriseSunsetData.map((sunriseSunsetDay, idx) => {
          const timezoneDay = this.state.dayData[idx];
          return {...sunriseSunsetDay, ...timezoneDay};
        });
        this.setState({
          ...this.state,
          dayData
        });
        //persist
        hasSessionStorage && sessionStorage.setItem(consts.TABLE_DATA, JSON.stringify(dayData));
        console.log('dayData', this.state.dayData);
        //now it's time to render the table
        //return the form to submit state for future use
        this.setState({
          ...this.state,
          requestsInProgress:false,
          navigateFromSubmit:true
        });
      })
      .catch((e) => this.showErrorNotification());
  }
  
  /*
  ** kludge, because I haven't studied this enough - pass in an event that won't be used,
  ** but helps distinguish unwanted references to the app component, keeping only references to the entry form
  */
  //TODO - add some kind of validation
  checkFormIsValid(e, formData) {
    //let addrFormIsValid = false;
    let addrFormIsValid = true; //TODO - actually check validity
    if (!e) {
      addrFormIsValid = true;
    }
    console.log('checkformisvalid form event:', e);
    console.log('checkformisvalid form?:', formData);
    // console.log('checkformisvalid form state:', form.state.formControls);
    // console.log('checkformisvalid text input refs', form.formControls);
    this.setState({
      ...this.state,
      addrFormIsValid
    });
  }
  
  enableReturnToForm() {
    this.setState({
      ...this.state,
      navigateFromSubmit:false
    });
  }
  
  //render the app
  //TODO - hideContent is for development only
  render() {
    return this.state.hideContent ? <div></div> : (
      <Router>
        <div>
          <a id="skippy" className="sr-only sr-only-focusable" href="#content">
            <div className="container">
              <span className="skiplink-text">Skip to main content</span>
            </div>
          </a>
          <Navbar inverse fixedTop>
            <Grid>
              <Navbar.Header>
                <Navbar.Brand>
                  {/*<img src={logo} className="App-logo" alt="logo" />*/}
                  <a href="/">Renew Financial</a>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
            </Grid>
          </Navbar>
          <Jumbotron>
            <Grid>
              <h1>Apparent solar time calculator</h1>
              <p className="App-intro">
                Calculate sunlight exposure for your solar installation
              </p>
            </Grid>
          </Jumbotron>
          <div className="App-main" id="content" role="main">
            <Grid>
              <Route exact={true} path="/" render={({history, location}) => (<EntryForm
                  history={history}
                  location={location}
                  dataErrors={this.state.dataErrors}
                  showErrorNotification={this.state.showErrorNotification}
                  hideErrorNotification={this.hideErrorNotification}
                  requestsInProgress={this.state.requestsInProgress}
                  navigateFromSubmit={this.state.navigateFromSubmit}
                  formValidates={this.state.addrFormIsValid}
                  checkFormIsValid={this.checkFormIsValid}
                  onSubmit={this.onSubmit}
                  useGeolocation={this.useGeolocation}
                  hasChosenGeoOrForm={this.hasChosenGeoOrForm}
                  locationCapable={this.locationCapable} />
              )} />
              <Route path="/results" render={() => (<SolarTimeTable
                dayData={this.state.dayData}
                enableReturnToForm={this.enableReturnToForm}
              />)} />
            </Grid>
          </div>
          <footer className="App-footer" id="footer">
            <Navbar>
              <p className="footer-block">Apparent solar time data provided by <a href="https://sunrise-sunset.org/api" target="_blank" rel="noopener noreferrer">Sunrise-Sunset</a></p>
            </Navbar>
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
