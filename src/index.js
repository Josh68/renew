import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
import 'bootstrap-sass/assets/stylesheets/bootstrap/_theme.scss';
import './styles/index.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
