/*
* Helpers for api calls
* */

import axios from 'axios';
import jsonp from 'jsonp';
import consts from '../App.constants';
import moment from 'moment';

export function geocodeTransformAddressString(string) {
  return string && string.trim().replace(/\s+/,'+');
}

export function getGeocoding(data) {
  return axios.get(consts.APIS.GOOGLE.GEOCODING.GET_URL(data[consts.ADDRESS_FIELDS.STREET],data[consts.ADDRESS_FIELDS.CITY],data[consts.ADDRESS_FIELDS.STATE],consts.APIS.GOOGLE.KEY), {
    transformResponse: (response) => JSON.parse(response).results[0].geometry.location
  });
}

export function getTimezone(location, _day) {
  const day = moment.isMoment(_day) ? _day : (_day ? moment(day) : moment());
  const timestamp = day.unix();
  return axios.get(consts.APIS.GOOGLE.TIMEZONE.GET_URL(location,timestamp,consts.APIS.GOOGLE.KEY), {
    //TODO - evaluate what to do with this
    transformResponse: (response) => {
      const combinedResponse = {...JSON.parse(response), location};
      return combinedResponse;
    }
  });
}

export function getSunriseSunset(location, date, callback) {
  //return axios.get(consts.APIS.SUNRISE_SUNSET.GET_URL(location.lat,location.lng,callback));
  jsonp(consts.APIS.SUNRISE_SUNSET.GET_URL(location.lat, location.lng, date), callback);
}