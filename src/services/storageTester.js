/*
* Test existence of storage APIs
* */

export const hasLocalStorage = (function() {
    if (!window.localStorage) {
      return false;
    }
    try {
      localStorage.setItem('foo', 'bar');
      localStorage.removeItem('foo');
      return true;
    } catch (exception) {
      return false;
    }
}());

export const hasSessionStorage = (function() {
  if (!window.sessionStorage) {
    return false;
  }
  try {
    sessionStorage.setItem('foo', 'bar');
    sessionStorage.removeItem('foo');
    return true;
  } catch (exception) {
    return false;
  }
}());
