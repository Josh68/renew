/**
 * getFormattedDate utility takes timestamp and returns string in format of 'YYYY-MM-DD'
 * @param timestamp
 * @returns {*|string}
 */

import Moment from 'moment';
import { extendMoment } from 'moment-range';
import * as momentDurationFormat from 'moment-duration-format';

const moment = extendMoment(Moment);

export function getFormattedDate(timestamp) {
  return timestamp.getFullYear && //test that this is a time object with appropriate methods
    timestamp.getFullYear() + '-' +
    zeroPadValue((timestamp.getMonth() + 1)) + '-' +
    zeroPadValue(timestamp.getDate())
}

function zeroPadValue(string) {
  return parseInt(string, 10) < 10 ? ('0' + string) : string;
}

export function getFormattedTimeFromSeconds(time) {
  return getFormattedTimeFromMilliseconds(time * 1000);
}

export function getFormattedTimeFromMilliseconds(time) {
  return moment.duration(time).format('h:mm:ss');
}

export function getOffsetTime(timestamp, offset) {
  window.moment = moment;
  let hourOffset = parseInt(offset,10)/60/60;
  const addOrSubtract = hourOffset > 0 ? 'add' : 'subtract';
  hourOffset = hourOffset > 0 ? hourOffset : (hourOffset - (hourOffset * 2));
  return moment(timestamp).utc()[addOrSubtract](hourOffset, 'hours').format('h:mm:ss A');
}

export function getRfNauticalAfternoon(solarNoon, nauticalTwilightEnd) {
  const sn = moment(solarNoon);
  const nte = moment(nauticalTwilightEnd);
  const range = moment.range(sn,nte);
  const fullDuration = range.duration('hours', true);
  console.log('solarNoon', solarNoon, 'nauticalTwilightEnd', nauticalTwilightEnd, 'range', range, 'fullDuration', fullDuration);
  return parseFloat(fullDuration).toPrecision(2).toString() + ' hrs';
}