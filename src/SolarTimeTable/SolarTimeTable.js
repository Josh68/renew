import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import moment from 'moment';
import {getFormattedTimeFromSeconds, getOffsetTime, getRfNauticalAfternoon} from "../services/dateTimeUtils";

//stateless or wrap? data is being manipulated inside, which it shouldn't be. Needs a HOC wrapper that preserves state and persists

class SolarTimeTable extends Component {
  componentDidMount() {
    this.props.enableReturnToForm();
  }
  render() {
   return (
     <Table striped condensed hover>
       <thead>
       <tr>
         <th>Date</th>
         <th>Sunrise</th>
         <th>Sunset</th>
         <th>rfNauticalAfternoon</th>
         <th>Day Length</th>
       </tr>
       </thead>
       <tbody>
       {/*in lieu of a real key, index should suffice for this demo*/}
       {/*sorting seems unnecessary, as it looks like it's provided by the datepicker*/}
       {
         this.props.dayData.map((day, idx) => {
             day.dayAsMoment = moment.isMoment(day.dayAsMoment) ? day.dayAsMoment : moment(day.dayAsMoment);
             const offset = day.rawOffset + day.dstOffset;
             return (<tr key={idx}>
               <td>{day.dayAsMoment.format('MM/DD/YYYY')}</td>
               <td>{getOffsetTime(day.sunrise, offset)}</td>
               <td>{getOffsetTime(day.sunset, offset)}</td>
               <td>{getRfNauticalAfternoon(day.solar_noon, day.nautical_twilight_end)}</td>
               <td>{getFormattedTimeFromSeconds(day.day_length)}</td>
             </tr>);
           }
         )
       }
       </tbody>
     </Table>
   );
  }
}

export default SolarTimeTable

// export default function SolarTimeTable(props) {
//   return(
//     <Table striped condensed hover>
//       <thead>
//       <tr>
//         <th>Date</th>
//         <th>Sunrise</th>
//         <th>Sunset</th>
//         <th>rfNauticalAfternoon</th>
//         <th>Day Length</th>
//       </tr>
//       </thead>
//       <tbody>
//         {/*in lieu of a real key, index should suffice for this demo*/}
//         {/*sorting seems unnecessary, as it looks like it's provided by the datepicker*/}
//         {
//           props.dayData.map((day, idx) => {
//               day.dayAsMoment = moment.isMoment(day.dayAsMoment) ? day.dayAsMoment : moment(day.dayAsMoment);
//               const offset = day.rawOffset + day.dstOffset;
//               return (<tr key={idx}>
//                 <td>{day.dayAsMoment.format('MM/DD/YYYY')}</td>
//                 <td>{getOffsetTime(day.sunrise, offset)}</td>
//                 <td>{getOffsetTime(day.sunset, offset)}</td>
//                 <td>{getRfNauticalAfternoon(day.solar_noon, day.nautical_twilight_end)}</td>
//                 <td>{getFormattedTimeFromSeconds(day.day_length)}</td>
//               </tr>);
//             }
//           )
//         }
//         </tbody>
//     </Table>
//   );
// }