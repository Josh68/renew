/*
* Application constants and constant getters
 */

import {geocodeTransformAddressString} from './services/apiService';

const consts = {
  USE_GEOLOCATION:'USE_GEOLOCATION',
  START_DATE:'startDate',
  END_DATE:'endDate',
  FORMATTED_START_DATE:'formattedStartDate',
  FORMATTED_END_DATE:'formattedEndDate',
  DATE_FORMAT:'YYYY-MM-DD',
  HORIZONTAL_ORIENTATION:'horizontal',
  VERTICAL_ORIENTATION:'vertical',
  VERTICAL_SCROLLABLE:'verticalScrollable',
  ANCHOR_LEFT:'left',
  ANCHOR_RIGHT:'right',
  OPEN_DOWN: 'down',
  OPEN_UP:'up',
  ADDRESS_FIELDS: {
    STREET:'addrStreet',
    CITY:'addrCity',
    STATE:'addrState',
    ZIP:'addrZip'
  },
  APIS: {
    GOOGLE: {
      KEY:'AIzaSyDp2X8GsrQQOQZzYmqs_vegkjKSPzsSwlk',
      GEOCODING: {
        SAMPLE:'https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=',
        GET_URL:(street, city, state, key) => `https://maps.googleapis.com/maps/api/geocode/json?address=${geocodeTransformAddressString(street)},+${geocodeTransformAddressString(city)},+${geocodeTransformAddressString(state)}&key=${key}`
      },
      TIMEZONE: {
        SAMPLE:'https://maps.googleapis.com/maps/api/timezone/json?location=38.908133,-77.047119&timestamp=1458000000&key=YOUR_API_KEY',
        GET_URL:({lat, lng}, timestamp, key) => `https://maps.googleapis.com/maps/api/timezone/json?location=${lat},${lng}&timestamp=${timestamp}&key=${key}`
      }
    },
    SUNRISE_SUNSET: {
      GET_URL:(lat, lng, date) => `https://api.sunrise-sunset.org/json?lat=${lat}&lng=${lng}&date=${date}&formatted=0`,
    }
  },
  TABLE_DATA: 'table_data'
};

export default consts;