# Development and installation notes

This project uses a modified react-scripts directory within a project using [a fork of create-react-app](https://github.com/josh68/create-react-app).

This approach is documented on a [Medium blog post](https://medium.com/@denis.zhbankov/maintaining-a-fork-of-create-react-app-as-an-alternative-to-ejecting-c555e8eb2b63) by Denis Zhbankov

After installing using my fork, at the project root, I did

```yarn add --dev @josh68/react-scripts```

which is a public, scoped package on npm.

The project includes support for SASS, but I did not manage to get source maps working

Several dependencies were added during development, but all should be installed correctly by following the instructions below.

# Installing

The app was developed using [Node current (v8.40, as of this time)](https://nodejs.org/en/), which includes NPM 5.3.0.

The app is versioned with [Git](https://git-scm.com/), which must be installed locally for development and contribution.

However, I've found it best to use [Yarn](https://yarnpkg.com/en/) with __create-react-app__. NPM commands seem to be having problems (may be filesystem dependent). I have the latest yarn installed globally.

For issues, use [RimRaf](https://www.npmjs.com/package/rimraf) and do ```rimraf node_modules``` followed by ```yarn```

This project can be git [cloned from Bitbucket](https://bitbucket.org/Josh68/renew):

 ```git clone git clone https://Josh68@bitbucket.org/Josh68/renew.git```

 and then dependencies installed using ```yarn```. No guarantee that ```npm install``` will work, as it has been resulting in some issues on my system. The app is run using ```yarn start``` (```npm start``` also works), and tests run using ```yarn test```.

# Notes from the developer

* This app is far from polished, well-refactored, or tested (has almost no tests, given how much time it took for me just to meet other requirements, sorry)
* I could go into great detail about what I would do differently or change going forward, but it is what it is. I am happy to discuss these points, however.
* The code contains notes and commented out portions alluding to some of the issues and some abandoned paths.
* I am not entirely sure of the accuracy of the solar data, but did my best. It seems slightly off from sunrise-sunset's own web app.

-----

Renew Financial: jsCodeChallenge
=================================

Hi there! We're ecstatic that you're interested in working for Renew Financial's dev team. To get a better idea of your current development skills, we'd like for you to complete a code challenge - build an application according to a set of requirements. The actual business requirements are listed further down, but here are the general tech requirements:

1. Create a single-page web application.
1. Make sure your app is runnable on Mac OS X or Linux.
1. Do not require any for-pay software.
1. Write tests for your code.
1. Include instructions for setting up and running your application locally (including versions for any installed software).

We currently use Angular, React/Redux, Enzyme, Jasmine, and Bootstrap but you may use any libraries or frameworks you wish.

Feel free to email us at [dstocker@renewfinancial.com](dstocker@renewfinancial.com) if you have any questions.

## Submission Instructions

1. Create a git repository.
1. Complete the project as described below within your local repository.
1. Create a .zip file with the contents of the your local repository (including hidden files & folders).
1. Email the .zip file to [dstocker@renewfinancial.com](dstocker@renewfinancial.com), and put the position you are applying for in the email's subject.

If you have any questions about this submission process, feel free to email us.

## Project Description

Renewable Funding has decided to enter the lucrative market of apparent-solar-time analysis.

It's a no brainer given the availability of APIs like these:

* http://sunrise-sunset.org/api
* https://developers.google.com/maps/documentation/geocoding/#geocoding
* https://developers.google.com/maps/documentation/timezone/intro

We want a user to be able to use our site as follows:

1. Enter an address and a date range
  1. Range of dates should default to the past seven days.
  1. Range of dates should not exceed 14 days.
1. See table-like presentation of sunrise time, sunset time, rfNauticalAfternoon, and day length where each row represents one day in the requested range.
  1. The table should be sorted by date with the most recent days on top.
  1. The times should reflect the time zone in which the address is located.
  1. rfNauticalAfternoon has been defined by our business people as the time elapsed from solar noon until the end of nautical twilight.

#### Bonus options:

If you're feeling inspired, bonus points for:

* enabling a user to save settings for the table view (e.g. a specific date range) with a url that can be linked to and display these settings.

## Evaluation

Reviewers will assess your familiarity with standard libraries and single-page app best practices.

1. Did your application fulfill the requirements?
1. Did you document the method for setting up and running your application (including versions for software that needs to be installed)?
1. Did you follow the instructions for submission?
1. Did your submission include test coverage?
1. Did you adhere to idiomatic javascript & conventional code patterns for the tools/frameworks that you used?


